shuf --random-source=/dev/urandom /usr/share/dict/words \
  | grep "^[[:lower:]]\{6,\}$" \
  | head
