PATH_ADDITIONS="$HOME/bin:$HOME/.local/bin:$HOME/.cabal/bin:/opt/haskell/bin"
case ":$PATH:" in
  *":$PATH_ADDITIONS:"*) :;;
  *) PATH="$PATH_ADDITIONS:$PATH";;
esac

# TODO: remove temp fix with new gnome-session version
# NOTE: will clobber SSH_AUTH_SOCK if being used by e.g. ssh-agent
if [ -r "/run/user/${UID}/keyring/ssh" ]; then
    export SSH_AUTH_SOCK="/run/user/${UID}/keyring/ssh"
fi

export EDITOR=vim
export LEDGER_FILE=~/ledger/main.ledger

# add color and source highlighting to less
export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
export LESS='--RAW-CONTROL-CHARS'

# time and size display formats for ls
export TIME_STYLE=long-iso
export BLOCK_SIZE="'1"

export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWSTASHSTATE=1
export GIT_PS1_SHOWUNTRACKEDFILES=1
export GIT_PS1_SHOWUPSTREAM="auto"
export GIT_PS1_DESCRIBE_STYLE="branch"
export GIT_PS1_SHOWCOLORHINTS=1
