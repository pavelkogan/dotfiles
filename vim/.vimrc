source ~/.exrc
source ~/.vim/bundles.vim

autocmd BufRead,BufNewFile *.md setfiletype markdown
autocmd BufRead,BufNewFile *.cljx setfiletype clojure

set background=dark
colorscheme solarized

set expandtab
set linebreak

set shiftround
set ignorecase
set smartcase

set gdefault  " invert 'g' for substitutions

set backup
set writebackup
set backupdir=~/.vim/backup,.,~/

set undofile
set undodir=~/.vim/undofiles,.

set autochdir
set hidden
set relativenumber

set mouse=a

let mapleader = "\<Space>"

nnoremap ; :

nmap <silent> <leader>ev :edit $MYVIMRC<CR>
nmap <silent> <leader>sv :source $MYVIMRC<CR>

" delete current buffer without closing split
nnoremap <leader>bd :b#<bar>bd#<cr>

nnoremap <leader>fs :w<cr>

" nul includes ctrl space
inoremap <nul> <esc>

let g:airline_powerline_fonts = 1

" toggle/close quickfix list
nnoremap <silent> <leader>qt :cw<cr>
nnoremap <silent> <leader>qc :ccl<cr>

call unite#filters#matcher_default#use(['matcher_fuzzy'])
nnoremap <leader>ff :Unite -no-split -start-insert file_rec/async:!<cr>
nnoremap <leader>bf :Unite -no-split buffer<cr>
nnoremap <leader>pf :UniteWithProjectDir -no-split -start-insert file_rec/async:!<cr>
nnoremap <leader>pg :Unite -no-split -start-insert grep<cr>
nnoremap <leader>/ :Unite grep:.<cr>

if executable('ag')
  let g:unite_source_grep_command = 'ag'
  let g:unite_source_grep_default_opts =
        \ '--line-numbers --nocolor --nogroup --ignore ' .
        \  '''.hg'' --ignore ''.svn'' --ignore ''.git'' --ignore ''.bzr'''
  let g:unite_source_grep_recursive_opt = ''
  let g:unite_source_rec_async_command =
        \ 'ag --nocolor --nogroup -g ""'
endif

nnoremap <silent> <F4> :NERDTreeToggle<CR>
nnoremap <silent> <F5> :GundoToggle<CR>

" <TAB>: completion.
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ neocomplete#start_manual_complete()
function! s:check_back_space() "{{{
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
  return pumvisible() ? neocomplete#close_popup() : "\<CR>"
endfunction

" let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
