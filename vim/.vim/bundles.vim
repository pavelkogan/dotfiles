set nocompatible
filetype off

set rtp+=~/.vim/bundle/neobundle.vim
call neobundle#begin(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/neocomplete.vim'
NeoBundleLazy 'scrooloose/nerdtree',
            \ {'autoload' : {'commands' : 'NERDTreeToggle'}}
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'tpope/vim-sensible'
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-abolish'
NeoBundle 'tpope/vim-speeddating'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'tpope/vim-unimpaired'
NeoBundle 'tpope/vim-sleuth'
NeoBundle 'bling/vim-airline'
NeoBundle 'michaeljsmith/vim-indent-object'
NeoBundle 'tomtom/tcomment_vim'
NeoBundle 'sjl/gundo.vim'
NeoBundle 'Valloric/ListToggle'
NeoBundle 'christoomey/vim-tmux-navigator'

NeoBundle 'bufexplorer.zip'
NeoBundle 'print_bw.zip'

call neobundle#end()

filetype plugin indent on

NeoBundleCheck
