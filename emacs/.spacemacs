;; -*- mode: dotspacemacs -*-

(defun dotspacemacs/layers ()
  "Configuration Layers declaration."
  (setq-default
   dotspacemacs-configuration-layer-path '()
   dotspacemacs-configuration-layers
   '(
     ansible
     auto-completion
     ;; better-defaults
     clojure
     csv
     docker
     emacs-lisp
     evil-commentary
     extra-langs
     finance
     (git :variables
          git-gutter-use-fringe t)
     github
     (haskell :variables
              haskell-completion-backend 'intero
              haskell-enable-hindent-style "johan-tibell")
     helm
     html
     idris
     ipython-notebook
     latex
     markdown
     (mu4e :variables
           mu4e-installation-path "/usr/local/share/emacs/site-lisp/mu4e")
     org
     pandoc
     python
     restclient
     scala
     shell
     shell-scripts
     sql
     syntax-checking
     terraform
     tmux
     vimscript
     yaml
     )
   dotspacemacs-additional-packages
   '(
     hasky-extensions
     hlint-refactor
     magithub
     ox-ioslide
     )
   dotspacemacs-excluded-packages '()
   dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration."
  (setq-default
   dotspacemacs-check-for-update t
   dotspacemacs-editing-style 'vim
   dotspacemacs-default-font '("Ubuntu Mono 12"
                               :powerline-scale 1.1)
   dotspacemacs-leader-key "SPC"
   dotspacemacs-emacs-leader-key "M-m"
   dotspacemacs-command-key ":"
   dotspacemacs-auto-resume-layouts t
   dotspacemacs-enable-paste-micro-state t
   dotspacemacs-fullscreen-at-startup t
   dotspacemacs-maximized-at-startup t
   dotspacemacs-line-numbers 'relative
   )
  )

(setq user/scale-factor
      (let ((dpi
             (string-to-number
              (shell-command-to-string "xrdb -query | grep dpi | cut -f2"))))
        (if (>= dpi 192) 2 1)))

(defmacro user/list-replace (l v w)
  `(setq ,l (mapcar (lambda (x) (if (equal x ,v) ,w x)) ,l)))

(defun dotspacemacs/user-config ()

  ;; prevent populating `package-selected-packages'
  (defun package--save-selected-packages (&rest opt) nil)

  (require 'helm-bookmark)  ;; temporary workaround

  ;; General
  ;; =======
  (setq vc-follow-symlinks t)
  (setq paradox-github-token t)  ;; don't ask about github integration
  (setq split-width-threshold 156)

  ;; prevent startup freeze when ISP hijacks DNS misses
  ;; this can be removed when the problem is fixed (should be in emacs 25.1.1)
  (setq tramp-ssh-controlmaster-options
        "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no")

  ;; Git
  ;; =====
  (use-package magithub
    :after magit
    :init (setq magithub-cache t)
    :config (magithub-feature-autoinject t))

  (magit-wip-after-save-mode 1)
  (magit-wip-after-apply-mode 1)

  ;; Email
  ;; =====
  (defun user/mu4e-view-set-width ()
    (setq mu4e-headers-visible-columns (- (window-total-width) 80)))
  (defun user/mu4e-force-update ()
    (mu4e-maildirs-extension-force-update '(16)))
  (defun user/mu4e-message-maildir-matches (x)
    (lexical-let ((regx x))
      (lambda (msg) (when msg (string-match regx (mu4e-message-field msg :maildir))))))

  (spacemacs|use-package-add-hook mu4e
    :post-config (evil-define-key 'evilified mu4e-main-mode-map "j" 'evil-next-line))
  (with-eval-after-load 'mu4e-headers
    (user/list-replace mu4e-headers-fields '(:subject) '(:thread-subject)))

  (add-hook 'mu4e-view-mode-hook #'visual-line-mode)
  (setq mu4e-split-view 'vertical)
  (user/mu4e-view-set-width)
  (setq shr-color-visible-luminance-min 60)
  (setq mu4e-headers-sort-direction 'descending)

  (with-eval-after-load 'mu4e
    (setq mu4e-contexts
          `( ,(make-mu4e-context
               :name "Gmail"
               :match-func (user/mu4e-message-maildir-matches "^/gmail")
               :vars '((user-mail-address . "pkogan@gmail.com")
                       (mu4e-sent-folder . "/gmail/sent")
                       (mu4e-trash-folder . "/gmail/trash")
                       (mu4e-drafts-folder . "/gmail/drafts")
                       (mu4e-sent-messages-behavior . delete)))
             ,(make-mu4e-context
               :name "MOL"
               :match-func (user/mu4e-message-maildir-matches "^/mol")
               :vars '((user-mail-address . "pavel.kogan@mailonline.co.uk")
                       (mu4e-sent-folder . "/mol/sent")
                       (mu4e-trash-folder . "/mol/trash")
                       (mu4e-drafts-folder . "/mol/drafts")
                       (mu4e-refile-folder . "/mol/archive")
                       (mu4e-sent-messages-behavior . sent))))))

  (setq mu4e-maildir "~/.mail"
        mu4e-context-policy 'pick-first
        mu4e-use-fancy-chars t
        mu4e-show-images t
        mu4e-change-filenames-when-moving t  ;; fixes duplicate UID error in mbsync
        mu4e-get-mail-command "mbsync -V -a"
        message-send-mail-function 'message-send-mail-with-sendmail)

  ;; Gitit
  ;; =====
  (add-to-list
   'magic-mode-alist
   '("---\\(.\\|\n\\)*format:[[:space:]]*org\\(.\\|\n\\)*\\.\\.\\.\n" . org-mode))
  (add-to-list 'auto-mode-alist '("\\.page\\'" . markdown-mode))

  ;; Org
  ;; ===
  (defun user/org-redisplay-inline-images ()
    (when org-inline-image-overlays
      (org-redisplay-inline-images)))

  (defun user/org-tangle ()
    (interactive)
    (when (eq major-mode 'org-mode)
      (org-babel-tangle)))

  (with-eval-after-load 'org
    (add-hook 'after-save-hook 'user/org-tangle)
    (plist-put org-format-latex-options :scale user/scale-factor)
    (let* ((imagemagick-string
            (pp-to-string
             (alist-get 'imagemagick org-preview-latex-process-alist)))
           (lua-imagemagick (read (replace-regexp-in-string
                                   "\"pdflatex\\>" "\"lualatex"
                                   imagemagick-string))))
      (evil-add-to-alist 'org-preview-latex-process-alist
                         'lua-imagemagick lua-imagemagick)))

  (add-hook 'org-babel-after-execute-hook 'user/org-redisplay-inline-images)
  (setq org-babel-load-languages '((emacs-lisp . t)
                                   (latex . t)
                                   (haskell . t))
        org-agenda-files '("~/org")
        org-startup-indented t
        org-preview-latex-default-process 'imagemagick)

  (evil-add-to-alist 'safe-local-variable-values
                     'org-preview-latex-default-process 'lua-imagemagick)

  ;; LaTeX
  ;; =====
  (setq preview-scale-function user/scale-factor)

  ;; Haskell
  ;; =======
  (evil-add-to-alist 'safe-local-variable-values
                     'haskell-indentation-where-pre-offset 4
                     'haskell-indentation-where-post-offset 4
                     'haskell-indentation-starter-offset 4
                     'haskell-indentation-left-offset 4
                     'haskell-indentation-layout-offset 4)
  (with-eval-after-load 'intero
    (flycheck-add-next-checker 'intero '(warning . haskell-hlint)))

  (spacemacs/set-leader-keys-for-major-mode 'haskell-mode "re" #'hasky-extensions)

  ;; Scala
  ;; =====
  (with-eval-after-load 'ensime
    (setq ensime-implicit-gutter-icons nil)
    (add-to-list 'ensime-sem-high-faces '(implicitConversion nil))
    (add-to-list 'ensime-sem-high-faces '(implicitParams nil)))

  ;; Fira Code
  ;; ==========

  ;; Uses Fira Code for Haskell operators and Fira Code Symbol for operator ligatures
  ;; Size should be set equal, except that :height in custom-set-faces is specified in tenths
  ;; (so it should be 10 times what is set in the font pattern string)

  ;; This works when using emacs --daemon + emacsclient
  (add-hook 'after-make-frame-functions (lambda (frame)
                                          (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol 10")))
  ;; This works when using emacs without server/client
  (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol 10")
  ;; I haven't found one statement that makes both of the above situations work, so I use both for now

  (defconst fira-code-font-lock-keywords-alist
    (mapcar (lambda (regex-char-pair)
              `(,(car regex-char-pair)
                (0 (prog1 ()
                     (compose-region (match-beginning 1)
                                     (match-end 1)
                                     ;; The first argument to concat is a string containing a literal tab
                                     ,(concat "	" (list (decode-char 'ucs (cadr regex-char-pair)))))))))
            '(("\\(www\\)"                   #Xe100)
              ("[^/]\\(\\*\\*\\)[^/]"        #Xe101)
              ("\\(\\*\\*\\*\\)"             #Xe102)
              ("\\(\\*\\*/\\)"               #Xe103)
              ("\\(\\*>\\)"                  #Xe104)
              ("[^*]\\(\\*/\\)"              #Xe105)
              ("\\(\\\\\\\\\\)"              #Xe106)
              ("\\(\\\\\\\\\\\\\\)"          #Xe107)
              ("\\({-\\)"                    #Xe108)
              ("\\(\\[\\]\\)"                #Xe109)
              ("\\(::\\)"                    #Xe10a)
              ("\\(:::\\)"                   #Xe10b)
              ("[^=]\\(:=\\)"                #Xe10c)
              ("\\(!!\\)"                    #Xe10d)
              ("\\(!=\\)"                    #Xe10e)
              ("\\(!==\\)"                   #Xe10f)
              ("\\(-}\\)"                    #Xe110)
              ("\\(--\\)"                    #Xe111)
              ("\\(---\\)"                   #Xe112)
              ("\\(-->\\)"                   #Xe113)
              ("[^-]\\(->\\)"                #Xe114)
              ("\\(->>\\)"                   #Xe115)
              ("\\(-<\\)"                    #Xe116)
              ("\\(-<<\\)"                   #Xe117)
              ("\\(-~\\)"                    #Xe118)
              ("\\(#{\\)"                    #Xe119)
              ("\\(#\\[\\)"                  #Xe11a)
              ("\\(##\\)"                    #Xe11b)
              ("\\(###\\)"                   #Xe11c)
              ("\\(####\\)"                  #Xe11d)
              ("\\(#(\\)"                    #Xe11e)
              ("\\(#\\?\\)"                  #Xe11f)
              ("\\(#_\\)"                    #Xe120)
              ("\\(#_(\\)"                   #Xe121)
              ("\\(\\.-\\)"                  #Xe122)
              ("\\(\\.=\\)"                  #Xe123)
              ("\\(\\.\\.\\)"                #Xe124)
              ("\\(\\.\\.<\\)"               #Xe125)
              ("\\(\\.\\.\\.\\)"             #Xe126)
              ("\\(\\?=\\)"                  #Xe127)
              ("\\(\\?\\?\\)"                #Xe128)
              ("\\(;;\\)"                    #Xe129)
              ("\\(/\\*\\)"                  #Xe12a)
              ("\\(/\\*\\*\\)"               #Xe12b)
              ("\\(/=\\)"                    #Xe12c)
              ("\\(/==\\)"                   #Xe12d)
              ("\\(/>\\)"                    #Xe12e)
              ("\\(//\\)"                    #Xe12f)
              ("\\(///\\)"                   #Xe130)
              ("\\(&&\\)"                    #Xe131)
              ("\\(||\\)"                    #Xe132)
              ("\\(||=\\)"                   #Xe133)
              ("[^|]\\(|=\\)"                #Xe134)
              ("\\(|>\\)"                    #Xe135)
              ("\\(\\^=\\)"                  #Xe136)
              ("\\(\\$>\\)"                  #Xe137)
              ("\\(\\+\\+\\)"                #Xe138)
              ("\\(\\+\\+\\+\\)"             #Xe139)
              ("\\(\\+>\\)"                  #Xe13a)
              ("\\(=:=\\)"                   #Xe13b)
              ("[^=!/]\\(==\\)[^=>]"           #Xe13c)
              ("[^=]\\(===\\)[^=]"                   #Xe13d)
              ("\\(==>\\)"                   #Xe13e)
              ("[^=]\\(=>\\)"                #Xe13f)
              ("\\(=>>\\)"                   #Xe140)
              ("\\(<=\\)"                    #Xe141)
              ("\\(=<<\\)"                   #Xe142)
              ("\\(=/=\\)"                   #Xe143)
              ("\\(>-\\)"                    #Xe144)
              ("\\(>=\\)"                    #Xe145)
              ("\\(>=>\\)"                   #Xe146)
              ("[^-=]\\(>>\\)"               #Xe147)
              ("\\(>>-\\)"                   #Xe148)
              ("\\(>>=\\)"                   #Xe149)
              ("\\(>>>\\)"                   #Xe14a)
              ("\\(<\\*\\)"                  #Xe14b)
              ("\\(<\\*>\\)"                 #Xe14c)
              ("\\(<|\\)"                    #Xe14d)
              ("\\(<|>\\)"                   #Xe14e)
              ("\\(<\\$\\)"                  #Xe14f)
              ("\\(<\\$>\\)"                 #Xe150)
              ("\\(<!--\\)"                  #Xe151)
              ("\\(<-\\)"                    #Xe152)
              ("\\(<--\\)"                   #Xe153)
              ("\\(<->\\)"                   #Xe154)
              ("\\(<\\+\\)"                  #Xe155)
              ("\\(<\\+>\\)"                 #Xe156)
              ("\\(<=\\)"                    #Xe157)
              ("\\(<==\\)"                   #Xe158)
              ("\\(<=>\\)"                   #Xe159)
              ("\\(<=<\\)"                   #Xe15a)
              ("\\(<>\\)"                    #Xe15b)
              ("[^-=]\\(<<\\)"               #Xe15c)
              ("\\(<<-\\)"                   #Xe15d)
              ("\\(<<=\\)"                   #Xe15e)
              ("\\(<<<\\)"                   #Xe15f)
              ("\\(<~\\)"                    #Xe160)
              ("\\(<~~\\)"                   #Xe161)
              ("\\(</\\)"                    #Xe162)
              ("\\(</>\\)"                   #Xe163)
              ("\\(~@\\)"                    #Xe164)
              ("\\(~-\\)"                    #Xe165)
              ("\\(~=\\)"                    #Xe166)
              ("\\(~>\\)"                    #Xe167)
              ("[^<]\\(~~\\)"                #Xe168)
              ("\\(~~>\\)"                   #Xe169)
              ("\\(%%\\)"                    #Xe16a)
              ;;("\\(x\\)"                     #Xe16b)
              ("[^:=]\\(:\\)[^:=]"           #Xe16c)
              ("[^\\+<>]\\(\\+\\)[^\\+<>]"   #Xe16d)
              ("[^\\*/<>]\\(\\*\\)[^\\*/<>]" #Xe16f))))

  (defun add-fira-code-symbol-keywords ()
    (font-lock-add-keywords nil fira-code-font-lock-keywords-alist))

  (add-hook 'haskell-mode-hook #'add-fira-code-symbol-keywords)
  (add-hook 'idris-mode-hook #'add-fira-code-symbol-keywords)
  )

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(haskell-operator-face ((t (:inherit font-lock-variable-name-face :height 100 :family "Fira Code"))))
 '(idris-hole-face ((t (:inherit idris-identifier-face :underline t))))
 '(idris-operator-face ((t (:inherit font-lock-variable-name-face :height 100 :family "Fira Code"))))
 '(idris-semantic-bound-face ((t (:slant italic))))
 '(idris-semantic-data-face ((t (:inherit font-lock-constant-face))))
 '(idris-semantic-function-face ((t (:inherit font-lock-function-name-face))))
 '(idris-semantic-namespace-face ((t (:inherit font-lock-type-face :slant italic :weight normal))))
 '(idris-semantic-type-face ((t (:inherit font-lock-type-face)))))
