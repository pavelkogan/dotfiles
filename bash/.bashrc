export CDPATH=.:~

# don't add commands of 3 characters or less to history
export HISTIGNORE='?:??:???:exit'

export HISTCONTROL='ignoreboth:erasedups'
export HISTFILESIZE=1000000
export HISTSIZE=1000000
export HISTTIMEFORMAT='%F %T  '

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# only displays when there are available security packages
# requires that available package files in /tmp be updated after apt update
# trigger /usr/lib/update-notifier/apt-check in /etc/apt/apt.conf.d/ (see 15update-stamp)
function show_updateable_packages() {
    local APT_PERIODIC_DIR UPDATE_TIME
    APT_PERIODIC_DIR=/var/lib/apt/periodic
    if [ "$(cut -d';' -f2 < $APT_PERIODIC_DIR/available-updates)" != "0" ]; then
        tput setaf 10
        cat $APT_PERIODIC_DIR/available-updates-readable
        if command -v timelapse >/dev/null 2>&1; then
            UPDATE_TIME=$(stat -c %Y $APT_PERIODIC_DIR/update-success-stamp | timelapse)
            echo last checked "$UPDATE_TIME"
        fi
        tput sgr0
    fi
}
show_updateable_packages && unset show_updateable_packages

source /usr/lib/git-core/git-sh-prompt
function set_prompt () {  #  $1 should be return value of previous command
    local VIRTUAL_ENV_PROMPT PWD_HOST PWD_PROMPT
    if [[ $1 == 0 ]]; then
        PS1="\[$(tput setaf 2)\]"
    else
        PS1="\[$(tput setaf 1)\]"
    fi
    PS1+=" \$ \[$(tput sgr0)\]"
    if [[ -n $VIRTUAL_ENV ]]; then
        VIRTUAL_ENV_PROMPT="\[$(tput setaf 10)\][$(basename "$VIRTUAL_ENV")]\[$(tput sgr0)\] "
    else
        VIRTUAL_ENV_PROMPT=""
    fi
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        PWD_HOST="\h:"
    else
        PWD_HOST=""
    fi
    PWD_PROMPT="\[$(tput setaf 5)\]$PWD_HOST\w\[$(tput sgr0)\]"
    __git_ps1 "$VIRTUAL_ENV_PROMPT$PWD_PROMPT" "$PS1"
}

# whenever displaying the prompt, write the previous line to disk
export PROMPT_COMMAND='
    set_prompt $?;
    echo -ne "\033]2;$(basename "`pwd`") - $TERM\007";
    history -a'

export PROJECT_HOME=~/work
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
[[ -f ~/.local/bin/virtualenvwrapper.sh ]] && source ~/.local/bin/virtualenvwrapper.sh

source /usr/share/bash-completion/completions/git
[[ -f ~/.local/etc/hub.bash_completion.sh ]] && source ~/.local/etc/hub.bash_completion.sh

[[ -f ~/.bash_aliases ]] && source ~/.bash_aliases

# set LS_COLORS
[[ -f ~/.dircolors ]] && eval "$(dircolors ~/.dircolors)"

# set terminal emulator specific variables
[[ -f ~/.term ]] && source ~/.term

[ -f ~/.travis/travis.sh ] && source ~/.travis/travis.sh

eval "$(stack --bash-completion-script "$(which stack)" 2> /dev/null)"

# shopt documented in 'man bash' -> '/^\s*shopt \['
shopt -s autocd
shopt -s cdspell
shopt -s checkjobs
shopt -s cmdhist
shopt -s dirspell
shopt -s extglob
shopt -s globstar
shopt -s histappend

# prevent Ctrl-D from exiting the shell
set -o ignoreeof
