alias ls='\ls --color=auto'
alias l='ls -hF'
alias ll='l -l'

alias grep='\grep --color=auto'

alias tmux='TERM=screen-256color tmux'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

alias hist=history
alias histt='history | tail'
alias histl='history | less'
alias histg='history | grep'

alias v=vim
alias m=man
alias exp=export
alias n=notes
alias t=tree
alias x='atool -x'
alias b=./build

alias userctl='systemctl --user'

alias c=cabal
alias cc='cabal configure --disable-library-profiling'
alias ci='cabal install'
alias cid='cabal install --dependencies-only'
alias cb='cabal build'
alias ccb='cc && cb'
alias cr='cabal run'
alias crepl='cabal repl --ghc-options="-ignore-dot-ghci -fdefer-type-errors"'
alias cs='cabal sandbox'
alias cbminim='cb --ghc-options=-ddump-minimal-imports'

alias s=stack
alias sb='stack build'
alias si='stack install'
alias se='stack exec'
alias sghci='stack ghci --ghci-options="-ignore-dot-ghci -fdefer-type-errors" --no-load'
alias sbminim='sb --ghc-options=-ddump-minimal-imports'

alias d=docker
alias ds='docker ps'
alias dr='docker run'
alias dri='docker run -it'
alias drim='docker run -it --rm'
alias dsta='docker start'
alias dsto='docker stop'
alias dre='docker restart'

alias dc=docker-compose

[[ -f ~/.git_aliases ]] && source ~/.git_aliases
